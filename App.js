import 'react-native-gesture-handler';
import React from 'react'
import { } from 'react-native'
import { NavigationContainer, DefaultTheme } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Login from './src/Screens/Login/Login';
import Register from './src/Screens/Register/Register';

import { Store, Persistor } from './src/Store/Store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'

const Stack = createStackNavigator();

const myTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white',
  }

}

export default function App() {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <NavigationContainer theme={myTheme}>
          <Stack.Navigator initialRouteName="Register">
            <Stack.Screen
              options={{ headerShown: false }}
              name="Login"
              component={Login} />
            <Stack.Screen
              options={{ headerShown: false }}
              name="Register"
              component={Register} />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}
