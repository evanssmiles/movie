import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-elements'
import { moderateScale } from 'react-native-size-matters'
import Feather from 'react-native-vector-icons/Feather'
import { widthPercentageToDP } from 'react-native-responsive-screen'


export default function Tombol(props) {
    return (
        <Button
            icon={<Feather style={{ marginRight: moderateScale(10) }} name="check" size={moderateScale(20)} color="#6372c3" />}
            title={props.judul}
            titleStyle={styles.textStyle}
            buttonStyle={styles.container}
            containerStyle={styles.shadow}
            onPress={props.onPress}
            disabled={props.disabled}
        />
    )
}

const styles = StyleSheet.create({
    textStyle: {
        color: '#6372c3'
    },

    container: {
        backgroundColor: 'white',
        borderRadius: widthPercentageToDP(50),

    },
    shadow: {
        elevation: 15,
        width: widthPercentageToDP(50),
        borderRadius: widthPercentageToDP(50),

    }


})
