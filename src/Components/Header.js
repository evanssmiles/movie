import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { moderateScale } from 'react-native-size-matters'

export default function Header(props) {
    const HeaderStyle = StyleSheet.create({
        container: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: widthPercentageToDP(8),
        },
        bigText: {
            fontSize: moderateScale(24),
            fontWeight: 'bold'
        },
        smallText: {
            fontSize: moderateScale(18),
            color: '#9c9c9c'
        },
    });

    return (
        <View style={HeaderStyle.container}>

            <Text style={HeaderStyle.bigText}>{props.bigTitle}</Text>


            <TouchableOpacity onPress={props.smallTextPress}>
                <Text style={HeaderStyle.smallText}>{props.smallTitle}</Text>
            </TouchableOpacity>
        </View>
    )
}
