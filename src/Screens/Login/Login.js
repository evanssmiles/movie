import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, Text, View, ScrollView } from 'react-native'
import Header from '../../Components/Header'
import Feather from 'react-native-vector-icons/Feather'
import { moderateScale } from 'react-native-size-matters'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { Input } from 'react-native-elements'
import Tombol from '../../Components/Button'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'

export default function Login(props) {

    const [passwordVisibility, setPasswordVisibility] = useState(false);


    return (
        <SafeAreaView style={styles.fullscreen}>
            <ScrollView

                contentContainerStyle={styles.styleContentContainer}>
                <Header
                    smallTextPress={() => props.navigation.navigate('Register')}
                    bigTitle="Login"
                    smallTitle="Sign Up"
                />
                <View style={styles.container}>
                    <View style={[styles.box, { marginTop: moderateScale(70) }]}>
                        <Feather
                            name="user"
                            size={moderateScale(80)}
                            color="#9c9c9c"
                        />

                        <Input placeholder="Username or Email Address" />
                        <Input
                            placeholder="Password"
                            rightIcon={
                                <TouchableOpacity
                                    activeOpacity={0.6}
                                    onPress={() => {
                                        setPasswordVisibility(!passwordVisibility)
                                    }}>
                                    <Feather
                                        name={passwordVisibility === true ? "eye-off" : "eye"}
                                        size={moderateScale(22)}
                                        color="#9c9c9c"

                                    />
                                </TouchableOpacity>
                            }
                            secureTextEntry={passwordVisibility}
                        />
                    </View>
                    <Tombol judul="Log In" />
                    <View style={styles.box}>
                        <Text style={styles.loginTextStyle}>Login With</Text>
                        <View style={styles.icons}>
                            <TouchableOpacity activeOpacity={0.6}>
                                <FontAwesome
                                    name="google-plus-official"
                                    color="#dc4e41"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <FontAwesome
                                    name="github"
                                    color="#5c6bc0"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <Entypo
                                    name="twitter-with-circle"
                                    color="#55abee"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <Entypo
                                    name="facebook-with-circle"
                                    color="#3b5998"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: widthPercentageToDP(8),
        flex: 1,
        justifyContent: 'space-between',
        height: heightPercentageToDP(100),

    },

    icons: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: widthPercentageToDP(84),
        marginTop: heightPercentageToDP(2),

    },
    fullscreen: {
        flex: 1,
        paddingVertical: heightPercentageToDP(4),
    },
    box: {
        width: widthPercentageToDP(84),
        alignItems: 'center',
    },

    loginTextStyle: {
        color: '#9c9c9c',
        fontSize: moderateScale(14),

    },

    styleContentContainer: {
        flexGrow: 1,

    },





})