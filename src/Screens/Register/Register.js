import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, Text, View, ScrollView, Alert, ToastAndroid } from 'react-native'
import Header from '../../Components/Header'
import Feather from 'react-native-vector-icons/Feather'
import { moderateScale } from 'react-native-size-matters'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { Input } from 'react-native-elements'
import Tombol from '../../Components/Button'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'



//redux
import { connect } from 'react-redux'

//action

import { changeUsernameOnRegisterReducer, changeEmail, changePassword } from './action'


function Register(props) {

    const [passwordVisibility, setPasswordVisibility] = useState(true);
    const [repeatPasswordVisibility, setRepeatPasswordVisibility] = useState(true);

    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [repeatPassword, setRepeatPassword] = useState("")
    const [errorPassword, setErrorPassword] = useState(false);



    return (
        <SafeAreaView style={styles.fullscreen}>
            <ScrollView

                contentContainerStyle={styles.styleContentContainer}>
                <Header
                    smallTextPress={() => props.navigation.navigate('Login')}
                    bigTitle="Sign Up"
                    smallTitle="Login"
                />
                <View style={styles.container}>
                    <View style={[styles.box, { marginTop: moderateScale(30) }]}>
                        <Feather
                            name="camera"
                            size={moderateScale(80)}
                            color="#9c9c9c"
                        />

                        <Input placeholder="Email Address" onChangeText={text => setEmail(text)} />
                        <Input placeholder="Username" onChangeText={text => setUsername(text)} />
                        <Input
                            placeholder="Password" onChangeText={text => setPassword(text)}
                            rightIcon={
                                <TouchableOpacity
                                    activeOpacity={0.6}
                                    onPress={() => {
                                        setPasswordVisibility(!passwordVisibility)
                                    }}>
                                    <Feather
                                        name={passwordVisibility === true ? "eye-off" : "eye"}
                                        size={moderateScale(22)}
                                        color="#9c9c9c"

                                    />
                                </TouchableOpacity>
                            }
                            secureTextEntry={passwordVisibility}
                        />
                        <Input
                            errorMessage={errorPassword ? "Password does not match" : null}
                            placeholder="Repeat Password"
                            onChangeText={text => setRepeatPassword(text)}
                            onBlur={() => {
                                if (repeatPassword !== password) {
                                    setErrorPassword(true);
                                } else {
                                    setErrorPassword(false);
                                }

                            }}
                            rightIcon={
                                <TouchableOpacity
                                    activeOpacity={0.6}
                                    onPress={() => {
                                        setRepeatPasswordVisibility(!repeatPasswordVisibility)
                                    }}>
                                    <Feather
                                        name={repeatPasswordVisibility === true ? "eye-off" : "eye"}
                                        size={moderateScale(22)}
                                        color="#9c9c9c"

                                    />
                                </TouchableOpacity>
                            }
                            secureTextEntry={repeatPasswordVisibility}
                        />
                    </View>
                    <Tombol onPress={() => {
                        props.changeUsernameOnRegisterReducer(username);
                        props.changeEmail(email)
                        props.changePassword(password)
                    }

                    }
                        judul="Sign Up"
                        disabled={errorPassword ? true : false}
                    />
                    <View style={styles.box}>
                        <Text style={styles.loginTextStyle}>Login With</Text>
                        <View style={styles.icons}>
                            <TouchableOpacity activeOpacity={0.6}>
                                <FontAwesome
                                    name="google-plus-official"
                                    color="#dc4e41"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <FontAwesome
                                    name="github"
                                    color="#5c6bc0"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <Entypo
                                    name="twitter-with-circle"
                                    color="#55abee"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.6}>
                                <Entypo
                                    name="facebook-with-circle"
                                    color="#3b5998"
                                    size={moderateScale(44)}
                                />
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}

//panggil degngan reduxmap
const mapStateToProps = (state) => ({
    globalState: state.GlobalState,
    Register: state.Register,
    Login: state.Login,
})

const mapDispatchToProps = {
    changeUsernameOnRegisterReducer,
    changeEmail,
    changePassword,

}


export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: widthPercentageToDP(8),
        flex: 1,
        justifyContent: 'space-between',
        height: heightPercentageToDP(100),

    },

    icons: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: widthPercentageToDP(84),
        marginTop: heightPercentageToDP(2),

    },
    fullscreen: {
        flex: 1,
        paddingVertical: heightPercentageToDP(4),
    },
    box: {
        width: widthPercentageToDP(84),
        alignItems: 'center',
    },

    loginTextStyle: {
        color: '#9c9c9c',
        fontSize: moderateScale(14),

    },

    styleContentContainer: {
        flexGrow: 1,

    },





})