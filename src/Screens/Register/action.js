export const changeUsernameOnRegisterReducer = username => {
    return {
        type: "SET_USERNAME_ON_REGISTER",
        payload: username,
    }
}

export const changeEmail = email => {
    return {
        type: "SET_EMAIL",
        payload: email,
    }
}

export const changePassword = password => {
    return {
        type: "SET_PASSWORD_ON_REGISTER",
        payload: password,
    }
}

