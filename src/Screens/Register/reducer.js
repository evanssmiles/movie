const initialState = {
    email: "",
    username: "",
    password: "",

}

const RegisterReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_EMAIL':
            return {
                ...state,
                email: action.payload,
            };
        case 'SET_USERNAME_ON_REGISTER':
            return {
                ...state,
                username: action.payload,
            };
        case 'SET_PASSWORD_ON_REGISTER':
            return {
                ...state,
                password: action.payload,
            };
        default:
            return state;
    }
};
export default RegisterReducer