import { createStore, applyMiddleware } from 'redux'
import { allReducers } from './allReducer'
import logger from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from "@react-native-async-storage/async-storage"
//middleware
//antar aplikasi -> debugger
//antar aplikasi -> REST API > API


// redux-logger

// bukan middleware
// redux-persist buat nyimpan data ke store walaupun aplikasi sudah di kill
// async storage
const persistConfig = {
    key: "Movie",
    storage: AsyncStorage,
}

const PersistedReducer = persistReducer(persistConfig, allReducers)


export const Store = createStore(PersistedReducer, applyMiddleware(logger));

export const Persistor = persistStore(Store)