import { combineReducers } from 'redux'
import GlobalReducer from './GlobalReducer'
import RegisterReducer from '../Screens/Register/reducer'
import LoginReducer from '../Screens/Login/reducer'

export const allReducers = combineReducers({
    GlobalState: GlobalReducer,
    Register: RegisterReducer,
    Login: LoginReducer

})